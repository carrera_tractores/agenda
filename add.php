<?php
session_start();
if (isset($_SESSION['session']) != null){  ?>

<!DOCTYPE html>
<html lang="ca">
    <?php include 'int/configBd.php'; ?>
<head>
<title><?php echo $siteName; ?> ~ Afegir usuari</title>
<meta charset="utf-8" />

<link rel="stylesheet" href="css/addCss.css" />
<link rel="shortcut icon" href="/favicon.ico" />
    <script type="text/javascript" src="js/validar.js"></script>
<link rel="alternate" title="Pozolería RSS" type="application/rss+xml" href="/feed.rss" />
</head>
<?php include 'int/header.php'; ?>

<nav>

    <?php include 'int/busca.php'; ?>
    <div id="addBotton"> <a href="principal.php"><img id="addBottonIco" src="icon/ic_keyboard_arrow_left_white_24dp.png" alt="add person"></a></div>
</nav>
<body onload="amaga()">

    <section>
        <h1> • Afegeix un usuari</h1>

        <div id="add">


          <?php if (!$_POST){ ?>
    <form id="login" name ="formularioContacto" onsubmit="return validacion()" method="post">
       <div id="fila1">

   <img src="icon/ic_account_circle_3x.png" id="avatar"><br/>
  <input type="text" name="nom" id="nom" value="" placeholder="Nom*" size=25/>
     <input type="text" name="cognom1" id="cognom1" value="" placeholder="Cognom" size=25/>
     <input type="text" name="cognom2" id="cognom1" value="" placeholder="Cognom" size=25/>

        </div>
         <div id="fila1">

     <img src="icon/ic_phone_3x.png"><br/>
            <input type="text" name="telefon" id="telefon" value="" placeholder="Telèfon*" size=12/></div>

        <div id="fila1">
             <img src="icon/ic_mail_outline_3x.png"><br/>
            <input type="text" name="email" id="email" value="" placeholder="Correu electrònic*" size=70/></div>

        <div id="fila1">
            <img src="icon/calendari.png" onclick="mostra()"><br/>
            <input type="text" name="dadaNaixement" id="dadaNaixement" value="" placeholder="Data de naixement*" onclick="mostra()" size=10/>
            <iframe id="iframe" src="calendari.html" width="200px" height="130px" scroll="none"></iframe>

            <script>
            function mostra() {
              parent.document.getElementById("iframe").style.display = "block";
            }

            function amaga() {
              parent.document.getElementById("iframe").style.display = "none";
            }
            </script>

            <input id="enviar" type="submit" value="Afegeix" />
            <input id="cancelar" type="button" onclick=" location.href='principal.php' " value="Cancel·la" name="boton" /> </div>


        <br/>
</form>
       <?php }else{

              if ($_POST['nom'] == null || $_POST['dadaNaixement']==null || $_POST['email'] == null || $_POST['telefon'] == null){
                  echo "Omple tots els camps obligatoris.";
                  header("refresh:2; url=add.php");
              }else{
                  mysqli_query($conn, "INSERT INTO contacte (nom,cognom1,cognom2,dadaNaixement,email,telefon,userId)VALUES( '$_POST[nom]','$_POST[cognom1]','$_POST[cognom2]',
                  '$_POST[dadaNaixement]','$_POST[email]','$_POST[telefon]','$_SESSION[session]')");
                  echo '<div class="alert alert-be">Els canvis s\'han guardat correctament. </div> ';
                    header("refresh:2; url=principal.php");
          }
       }
       ?>
        </div>
    </section>

    <?php include 'int/footer.php'; ?>
</body>
</html>
<?php }else{
    header("location:index.php");
}
?>
