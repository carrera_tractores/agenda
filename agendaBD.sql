
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agenda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacte`
--

CREATE TABLE `contacte` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `cognom1` varchar(50) NOT NULL,
  `cognom2` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `dadaNaixement` varchar(10) NOT NULL,
  `telefon` int(12) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacte`
--

INSERT INTO `contacte` (`id`, `nom`, `cognom1`, `cognom2`, `email`, `dadaNaixement`, `telefon`, `userId`) VALUES
(1, 'Usuari1', 'Cognom1', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 2),
(2, 'Usuari2', 'Cognom2', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(3, 'Usuari3', 'Cognom3', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(4, 'Usuari4', 'Cognom4', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(5, 'Usuari5', 'Cognom5', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(6, 'Usuari6', 'Cognom6', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(7, 'Usuari7', 'Cognom7', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(8, 'Usuari8', 'Cognom8', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(9, 'Usuari9', 'Cognom9', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(10, 'Usuari10', 'Cognom10', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(11, 'Usuari11', 'Cognom11', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(12, 'Usuari12', 'Cognom12', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(13, 'Usuari13', 'Cognom13', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(15, 'Usuari14', 'Cognom14', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(16, 'Usuari15', 'Cognom15', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(17, 'Usuari16', 'Cognom16', '', 'xarly.gl@gmail.com', '2016/12/02', 608611307, 1),
(18, 'AAAA', 'ZZZZ', '', 'xarly.gl@gmail.com', '6/11/2016', 608611307, 1),
(19, 'ZZZZ', 'AAAA', '', 'xarly.gl@gmail.com', '5/11/2016', 608611307, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuari`
--

CREATE TABLE `usuari` (
  `id` int(11) NOT NULL,
  `usuari` varchar(25) NOT NULL,
  `pass` varchar(900) NOT NULL,
  `email` varchar(70) NOT NULL,
  `img` varchar(100) NOT NULL DEFAULT 'img/user.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuari`
--

INSERT INTO `usuari` (`id`, `usuari`, `pass`, `email`, `img`) VALUES
(1, 'Carlos', '0985251f3d13076beec69aca778ea31f', 'xarly.gl@gmail.com', 'img/carlos.jpg'),
(2, 'Test', '0985251f3d13076beec69aca778ea31f', 'test@gmail.com', 'img/user.png'),
(3, 'Anna', '0985251f3d13076beec69aca778ea31f', 'anna.serra25@gmail.com', 'img/anna2.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacte`
--
ALTER TABLE `contacte`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuari`
--
ALTER TABLE `usuari`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacte`
--
ALTER TABLE `contacte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
