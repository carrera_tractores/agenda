<?php include 'int/configBd.php';
session_start();
if (isset($_SESSION['session']) != null){
if (!isset($_POST['buscar']) or $_POST['buscar'] == ""){
    echo "Camp buit";
    header("refresh:1; url=principal.php");
    exit;
}else{
$buscar = $_POST['buscar'];
$_pagi_sql = mysqli_query($conn, "SELECT nom,cognom1,cognom2,dadaNaixement,email,telefon,id FROM contacte where userId = '$_SESSION[session]' and nom LIKE '%$buscar%' OR cognom1 LIKE '%$buscar%' OR telefon LIKE '%$buscar%' ORDER BY nom");
$control = mysqli_num_rows($_pagi_sql);
?>
<!DOCTYPE html>
<html lang="ca">
<head>
    <title><?php echo $siteName; ?> ~ Afegir usuari</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="css/principalCss.css"/>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="alternate" title="Pozolería RSS" type="application/rss+xml" href="/feed.rss"/>
</head>
<?php include 'int/header.php'; ?>
<nav>
    <?php include 'int/busca.php'; ?>
    <div id="addBotton"><a href="principal.php"><img id="addBottonIco" src="icon/ic_keyboard_arrow_left_white_24dp.png" alt="add person"></a></div>
</nav>
<body>
<section>
    <h1>Resultats de: <?php echo $buscar; ?></h1>

    <?php if ($control == 0) { // establecemos un control de resultados, en caso de no obtener ningun resultado, saltará el error y el flujo de la página se parará.
        echo '<div class="alert alert-perill"> <strong>¡Opss...!  </strong>No s\'han trobat resultats.</div>';
        include 'int/footer.php';
        exit;
    } ?>
    <div class="Table">

        <div class="Heading">
            <div class="Cell">
                <p> Nom i Cognoms</p>
            </div>
            <div class="Cell">
                <p>Data</p>
            </div>
            <div class="Cell">
                <p>Telèfon</p>
            </div>
            <div class="Cell">
                <p>Email</p>
            </div>
            <div class="Cell">
                <p>Opcions</p>
            </div>
        </div>
        <?php
        while ($row = mysqli_fetch_array($_pagi_sql)) {
            ?>
            <div class="Row">
                <div class="Cell">
                    <p> <?php echo $row['nom'] . " " . $row['cognom1'] . " " . $row['cognom2']; ?></p>
                </div>
                <div class="Cell">
                    <p><?php echo $row['dadaNaixement']; ?></p>
                </div>
                <div class="Cell">
                    <p><?php echo $row['telefon']; ?></p>
                </div>
                <div class="Cell">
                    <p><?php echo $row['email']; ?></p>
                </div>
                <div class="Cell">
                    <p><a href=edit.php?id=<?php echo $row['id']; ?> ><img src="icon/ic_settings_2x.png" alt="edit"></a>
                        <a href=edit.php?id=<?php echo $row['id']; ?> ><img src="icon/ic_delete_forever_black_24dp_2x.png" alt="edit"></a></p>
                </div>
            </div>
            <?php
        }
        }
        } else {
            header("location:index.php");
        }
        ?>
</section>

<?php include 'int/footer.php'; ?>
</body>
</html>