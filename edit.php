<?php
session_start();
if (isset($_SESSION['session']) != null) {
    if ($_GET['id'] == null || $_GET['id'] == "0" || $_GET['id'] == "" || $_GET['id'] == 0) {
        //control de id
        header("location:principal.php");
        exit;
    } else {
        $id = (int)$_GET['id'];
        ?>
        <!DOCTYPE html>
        <html lang="ca">
        <head>
            <?php include 'int/configBd.php'; ?>
            <title> <?php echo $siteName; ?> ~ Edita</title>
            <meta charset="utf-8"/>
            <link rel="stylesheet" href="css/editCss.css"/>
            <link rel="shortcut icon" href="/favicon.ico"/>
            <script type="text/javascript" src="js/validar.js"></script>
            <link rel="alternate" title="Pozolería RSS" type="application/rss+xml" href="/feed.rss"/>
        </head>
        <?php include 'int/header.php'; ?>
        <nav>
            <?php include 'int/busca.php'; ?>
            <div id="addBotton"><a href="principal.php">
                    <img id="addBottonIco" src="icon/ic_keyboard_arrow_left_white_24dp.png" alt="back"></a></div>
        </nav>
        <body onload="amaga()">
        <section>
        <?php
        if (!$_POST) {
            $result = mysqli_query($conn, "SELECT nom,cognom1,cognom2,dadaNaixement,email,telefon,id FROM contacte WHERE id = $id and userId = '$_SESSION[session]'");
            $control = mysqli_num_rows($result); // miramos si $result obtiene resultados
            if ($control == 0) { // si no se obtiene ningún resultado, se redirecciona a principal
                header("location:principal.php");
                exit;
            }
            while ($row = mysqli_fetch_array($result)) {?>

                <h1> • Editant l'usuari: <?php echo $row['nom'] . " " . $row['cognom1'] . " " . $row['cognom2']; ?></h1>
                <div id="add">
                <form id="login" action="edit.php?id=<?php echo $row['id']; ?>" onsubmit="return validacion()" method="post">
                <div id="fila1">
                    <div id="bloque">
                        <img src="icon/ic_account_circle_3x.png" id="avatar"><br/>
                        <input type="text" name="nom" id="nom" value="<?php echo $row['nom']; ?>" size=25/>
                        <input type="text" name="cognom1" id="cognom1" value="<?php echo $row['cognom1']; ?>" size=25/>
                        <input type="text" name="cognom2" id="cognom2" value="<?php echo $row['cognom2']; ?>" size=25/>
                    </div>
                </div>

                <div id="fila1">
                    <div id="bloque" class="test2">
                        <img src="icon/ic_phone_3x.png"><br/>
                        <input type="text" name="telefon" id="telefon" value="<?php echo $row['telefon']; ?>" size=50/>
                    </div>

                </div>
                <div id="fila1">
                    <div id="bloque" class="izq">
                        <img src="icon/ic_mail_outline_3x.png"><br/>
                        <input type="text" name="email" id="email" value="<?php echo $row['email']; ?>" size=50/></div>
                </div>

                <div id="bloque">
                            <div id="fila1">
            <img src="icon/calendari.png" onclick="mostra()"><br/>
            <input type="text" name="dadaNaixement" id="dadaNaixement" value="<?php echo $row['dadaNaixement']; ?>" onclick="mostra()" size=10/>
            <iframe id="iframe" src="calendari.html" width="200px" height="130px" scroll="none"></iframe>

            <script>
            function mostra() {
              parent.document.getElementById("iframe").style.display = "block";
            }
            function amaga() {
              parent.document.getElementById("iframe").style.display = "none";
            }
            </script>
            <input id="enviar" type="submit" value="Afegeix" />
            <input id="cancelar" type="button" onclick=" location.href='principal.php' " value="Cancel·la" name="boton" /> </div>
                </div>
            <?php } //tanca while
            ?>
            <br/>
            <?php
        } else { // if  de edicion
            $id2 = $_GET['id'];
            mysqli_query($conn, "Update contacte Set nom='$_POST[nom]', cognom1='$_POST[cognom1]', cognom2='$_POST[cognom2]',dadaNaixement='$_POST[dadaNaixement]',email='$_POST[email]', telefon='$_POST[telefon]' Where id=$id2");
            echo '<div class="alert alert-be">Els canvis s\'han guardat correctament.</div>';
            header("refresh:2; url=principal.php");
        }
    }
    ?>
    </form>
    </div>
</section>
    <?php include 'int/footer.php'; ?>
</body>
    </html>
<?php } else {
    header("location:index.php");
}
?>
