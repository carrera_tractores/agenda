<?php
session_start();
if (isset($_SESSION['session']) != null) {
    header("location:principal.php");
} else {
    ?>
    <!DOCTYPE html>
    <html lang="ca">
    <head>
        <?php include 'int/configBd.php'; ?>
        <title><?php echo $siteName; ?> ~ Agenda</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="css/indexCss.css"/>
        <script type="text/javascript" src="js/validarLogin.js"></script>
        <link rel="shortcut icon" href="/favicon.ico"/>
        <link rel="alternate" title="Pozolería RSS" type="application/rss+xml" href="/feed.rss"/>

    </head>
    <body>
    <?php include 'int/header.php'; ?>
    <section>
        <?php if (!$_POST) { ?>
            <div id="formulario">
                <form id="login" action="index.php" method="POST" onsubmit="return validacionLogin()">
                    <div id="cajaElemento">
                        <img src="icon/ic_account_circle_3x.png" id="avatar"><br/>
                        <input type="text" id="email" name="email" value="" placeholder="Email" size=58/>
                    </div>
                    <img src="icon/ic_https_3x.png"><br/>
                    <input type="password" name="pass" id="pass" value="" placeholder="Contrasenya" size=58/><br/><br/>
                    <br/>
                    <br/>
                    <input type="submit" value="Enviar"/>
                </form>

            </div>
        <?php } else {
            $_email = 0;
            $_pass = 0;
            $email = $_POST['email'];
            $pass = $_POST['pass'];
            $pass = md5($pass); // convertimos la contraseña que nos pasa el user en md5
            $_login = mysqli_query($conn, "SELECT email,pass,id,usuari,img FROM usuari WHERE email = '$email' and pass = '$pass'");
            while ($consulta = mysqli_fetch_array($_login)) {
                $_email = 1;
                $_pass = 1;
                $_userid = $consulta['id'];
                $_user = $consulta['usuari'];
                $_img = $consulta['img'];
            }
            if ($_email == 0 || $_pass == 0) {
                echo '<div class="alert alert-malament"> <strong>Error: </strong>Torneu a intentar-ho.</div>';
                header("refresh:2; url=index.php");
            } else {
                session_start();
                $_SESSION['session'] = $_userid;
                $_SESSION['nameSession'] = $_user;
                $_SESSION['imgSession'] = $_img;
                header("location:principal.php");
            }
        }
        ?>
    </section>
    <?php include 'int/footer.php'; ?>
    </body>
    </html>
<?php } ?>
