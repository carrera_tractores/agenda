<header>
    <svg id="imgLogo" fill="#FFF" height="200px" viewBox="0 0 24 24" width="200px" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0h24v24H0z" fill="none"/>
        <circle cx="7.2" cy="14.4" r="3.2"/>
        <circle cx="14.8" cy="18" r="2"/>
        <circle cx="15.2" cy="8.8" r="4.8"/>
    </svg>
    <span id="logo"><?php echo $siteName; ?></span>
    <?php if (isset($_SESSION['session']) != null) { ?>
        <div id="user_data">
            <h3> Hola <?php echo $_SESSION["nameSession"]; ?>!</h3>
            <a title="out" href="out.php"><img src=icon/ic_settings_power_2x.png alt="out"/></a>
        </div>
        <img id="userImg" src="<?php echo $_SESSION["imgSession"]; ?>" width="209px" height="207px" alt="imgUser"/>
    <?php } else {
    } ?>
</header>
