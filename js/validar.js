function validacion() { // Miramos campo por campo, si los valores obligatorios han sido introducidos

        var comodin = true;
        var validarEmail =  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        var errores = '';
        
    if (document.getElementById("nom").value == null || document.getElementById("nom").value == 0 || /^\s+$/.test(document.getElementById("nom").value)){
		errores+='\n - Nom: Camp buit.';
        comodin = false;
    }
    
    if (document.getElementById("dadaNaixement").value == null || document.getElementById("dadaNaixement").value == 0){
        comodin = false;
        errores+='\n - Data de naixement: Camp buit';
    }
    if (document.getElementById("email").value == null || document.getElementById("email").value == 0){
        comodin = false;
        errores+='\n - Email: Camp buit';
    }
    
    if (!validarEmail.test(document.getElementById("email").value)){
		errores+='\n - Email: format incorrecte.';
		comodin = false;
	}
    if (document.getElementById("telefon").value == null || document.getElementById("telefon").value == 0){
        comodin = false;
        errores+='\n - Telèfon: Camp buit';
    }
    if (comodin == true){
        return true;
    }else{
    alert (errores);
    return false;


}
}


