<?php
session_start();
if (isset($_SESSION['session']) != null) { ?>
    <!DOCTYPE html>
    <html lang="ca">
    <head>
        <?php include 'int/configBd.php'; ?>
        <title><?php echo $siteName; ?> ~ Agenda</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="css/principalCss.css"/>
        <link rel="shortcut icon" href="/favicon.ico"/>
        <link rel="alternate" title="Pozolería RSS" type="application/rss+xml" href="/feed.rss"/>
        <script type="text/javascript" src="js/delt.js"></script>
    </head>
    <body>
        <?php include 'int/header.php'; ?>
        <nav>
            <?php include 'int/busca.php'; ?>
            <div id="addBotton"><a href="add.php"><img id="addBottonIco" src="icon/ic_person_add_white_3x.png" alt="add person"></a></div>
        </nav>
        <?php
        
        // le pasamos la id de la paginación a principal.php
        $start = 0;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $start = ($id - 1) * $limit;
        } else {
            $id = 1;
        }
            
        /* Control de orden */
       if (!isset($_GET['ordenar'])) {
		   $ordenar = "nom";
       }elseif (($_GET['ordenar']) == "nom") {
             $ordenar = $_GET['ordenar'];
        }elseif (($_GET['ordenar']) == "cognom1") { 
			$ordenar = $_GET['ordenar'];
		}elseif (($_GET['ordenar']) == "cognom2" ){ 
			$ordenar = $_GET['ordenar'];
		}else{
			$ordenar = "nom";
		}
       /*----------------------*/
        $_pagi_sql = mysqli_query($conn, "SELECT nom,cognom1,cognom2,dadaNaixement,email,telefon,id,userId FROM contacte where userId = '$_SESSION[session]'ORDER BY $ordenar LIMIT $start, $limit"); ?>
        <section>
            <?php include 'int/paginar.php'; ?>
            <div id="navegarPaginarLeft"> <?php echo $resulLeft // botón de paginación ?> </div>
            <div id="navegarPaginarRight"> <?php echo $resulRight  // botón de paginación ?> </div>
            <div class="Table">
                <div class="Heading">
                    <div class="Cell">
                        <p> <a class="links" href="principal.php?ordenar=nom">Nom</a> i <a class="links" href="principal.php?ordenar=cognom1">cognom</a></p>
                    </div>
                    <div class="Cell">
                        <p>Data</p>
                    </div>
                    <div class="Cell">
                        <p>Telèfon</p>
                    </div>
                    <div class="Cell">
                        <p>Email</p>
                    </div>
                    <div class="Cell">
                        <p>Opcions</p>
                    </div>
                </div>
                <?php
                $_contador = 0; // si no hay ningún resultado,
                while ($row = mysqli_fetch_array($_pagi_sql)) { ?>
                    <div class="Row">
                        <div class="Cell">
                            <p> <?php echo $row['nom'] . " " . $row['cognom1'] . " " . $row['cognom2']; ?></p>
                        </div>
                        <div class="Cell">
                            <p><?php echo $row['dadaNaixement']; ?></p>
                        </div>
                        <div class="Cell">
                            <p><?php echo $row['telefon']; ?></p>
                        </div>
                        <div class="Cell">
                            <p><?php echo $row['email']; ?></p>
                        </div>
                        <div class="Cell">
                            <p><a href=edit.php?id=<?php echo $row['id']; ?> ><img src="icon/ic_settings_2x.png" alt="edit"></a>
                                <a href=# onclick="delt(<?php echo $row['id']; ?>)"><img src="icon/ic_delete_forever_black_24dp_2x.png" )" alt="borrar"></a></p>
                        </div>
                    </div>
                    <?php
                    $_contador++;
                }
                if ($_contador == 0) { // Si el usuario no tiene ningún contacto, aparecerá esto
                    ?>
                    <div class="Row">
                        <div class="Cell">
                            <p>Usuari de demostració</p>
                        </div>
                        <div class="Cell">
                            <p>29/11/2016</p>
                        </div>
                        <div class="Cell">
                            <p>612345678</p>
                        </div>
                        <div class="Cell">
                            <p>demostracio@iam.cat</p>
                        </div>
                        <div class="Cell">
                            <p><a href="add.php">Afegeix usuari</a></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="barraPag">
            <?php
            foreach ($num_pag2 as $value) { // mostramos el número de páginas que se han generado.
                //echo $value;
                echo "<div id=\"numPag\"> $value</div>";
            }
            ?>
            </div>
        </section>
        <?php include 'int/footer.php'; ?>
    </body>
    </html>
<?php } else {
    header("location:index.php");
}
?>
