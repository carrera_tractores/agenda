1. Creamos una base de datos llamada: agenda
2. En la base de datos, importamos la sentencia SQL (el código se encuentra en: agendaBD.sql)
3. Ahora entramos en la carpeta int y abrimos el archivo configBd.php y lo configuramos. 
4. Listo! Todo configurado, para testear la aplicación entramos en nuestra web, y hacemos login con:
	
	email: xarly.gl@gmail.com
	pass: 123123a 
	---------------
	email: test@gmail.com
	pass: 123123a
	---------------
	email: anna.serra25@gmail.com
	pass: 123123a
	